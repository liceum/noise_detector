from machine import Timer, unique_id
from time import sleep_ms
from ubinascii import hexlify

from connection import Wlan
from datacollector import DataCollector
from gsocket import GracefulSocket as GSocket
from sound import SoundSensor
from temperature import PicoTempSensor

from config import SSID, PASS, HOST, PORT

is_jacek_pico = hexlify(unique_id()).decode('utf-8') == "e661385283325335"

wlan = Wlan()
wlan.connect(SSID, PASS)
wlan.pretty_print_status()
wlan.blink_status()
print("Connected! {}".format(wlan.ifconfig()))

sensor = PicoTempSensor() if is_jacek_pico else SoundSensor(pin=2)
data = DataCollector()

t1 = Timer()
t2 = Timer()

with GSocket() as gs:
    gs.connect((HOST, PORT))
    t1.init(period=10, mode=Timer.PERIODIC, callback=lambda t:data.store_value_and_calculate_avg(sensor.get_value()))
    sleep_ms(995)
    t2.init(period=1000, mode=Timer.PERIODIC, callback=lambda t:gs.sendall(data.get_json_value()))
    while True:
        pass
