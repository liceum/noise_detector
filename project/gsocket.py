import socket

class GracefulSocket(socket.socket):
    def __init__(self):
        super().__init__(socket.AF_INET, socket.SOCK_STREAM)

    def __enter__(self):
        return self

    def __exit__(self, *args):
        print("Closing socket")
        super().close()
        print("Socket closed")
