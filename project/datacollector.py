import json

class DataCollector():
    def __init__(self):
        self.reset()

    def reset(self):
        self.i = 0
        self.avg = 0
        self.max = 0
        self.min = 65535

    def store_value_and_calculate_avg(self, value):
        self.avg = ((self.avg * self.i) + value) / (self.i + 1)
        self.i += 1
        self.max = max(self.max, value)
        self.min = min(self.min, value)

    def get_json_value(self):
        ret_val = {
            "avg": self.avg,
            "max": self.max,
            "min": self.min,
            "meas": self.i
        }
        self.reset()
        return json.dumps(ret_val)
