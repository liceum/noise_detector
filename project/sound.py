from machine import ADC

class SoundSensor(ADC):
    def __init__(self, pin):
        super().__init__(pin)

    def get_value(self):
        return round(self.read_u16()/400, 1)
