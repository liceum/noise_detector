from machine import ADC

class PicoTempSensor(ADC):
    def __init__(self):
        super().__init__(4)

    def get_value(self):
        voltage = 3.3 * (self.read_u16() / 65535)
        return PicoTempSensor.pico_temp_conversion(voltage)

    @staticmethod
    def pico_temp_conversion(voltage):
        # Formula for calculating temp from voltage for the onboard temperature sensor
        return 27 - (voltage - 0.706)/0.001721
