from machine import Pin
from time import sleep
import network

LONG__SLEEP = 2.0
SHORT_SLEEP = 0.5

class Wlan(network.WLAN):
    def __init__(self):
        super().__init__(network.STA_IF)
        self.active(True)

    def connect(self, SSID, password):
        super().connect(SSID, password)
        # Wait for connection
        max_wait = 10
        while max_wait > 0:
            if self.status() < 0 or self.status() >= 3:
                break
            max_wait -= 1
            print("Waiting for connection...")
            sleep(1)

    def pretty_print_status(self):
        if self.status() == 0:
            print("network.STAT_IDLE")
        elif self.status() == 1:
            print("network.STAT_CONNECTING")
        elif self.status() == -3:
            print("network.STAT_WRONG_PASSWORD")
        elif self.status() == -2:
            print("network.STAT_NO_AP_FOUND")
        elif self.status() == -1:
            print("network.STAT_CONNECT_FAIL")
        elif self.status() == 3:
            print("network.STAT_GOT_IP")
        else:
            print("Unknown network status")

    def blink_status(self):
        #  0 = STAT_IDLE
        #  1 = STAT_CONNECTING
        # -3 = STAT_WRONG_PASSWORD
        # -2 = STAT_NO_AP_FOUND
        # -1 = STAT_CONNECT_FAIL
        #  3 = STAT_GOT_IP

        led = Pin('LED', Pin.OUT, value=0)
        if self.status() == network.STAT_GOT_IP:
            for i in range(3):
                led.value(1)
                sleep(LONG__SLEEP)
                led.value(0)
                sleep(SHORT_SLEEP)
            return
        while True:
            led.value(1)
            sleep(LONG__SLEEP)
            led.value(0)
            sleep(SHORT_SLEEP)
            for i in range(-1 * self.status()):
                led.value(1)
                sleep(SHORT_SLEEP)
                led.value(0)
                sleep(SHORT_SLEEP)
            led.value(1)
            sleep(LONG__SLEEP)
            led.value(0)
            sleep(LONG__SLEEP)
