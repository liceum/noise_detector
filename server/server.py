import json
import socket
import sys
import datetime

sys.path.append("../")
from project.config import HOST, PORT


if __name__ == "__main__":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
        print("Waiting for client...")
        conn, addr = s.accept()
        with conn:
            print(f"Connected by {addr}")
            while True:
                data = conn.recv(1024)
                if not data:
                    break
                print(data)
                print(datetime.datetime.now())
                decoded_data = data.decode('utf-8')
                decoded_data = decoded_data[:decoded_data.find('}') + 1]
                print(decoded_data)
                json_data = json.loads(decoded_data)
                print(json_data)
